<?php
/**
 * Created by PhpStorm.
 * User: AndrewBit
 * Date: 23/06/2016
 * Time: 20:16
 */

namespace QuickUHC;

use pocketmine\event\Listener;

class Events implements Listener {

    /** @var Base */
    private $plugin;

    /**
     * Events constructor.
     * @param Base $plugin
     */
    public function __construct(Base $plugin) {
        $this->plugin = $plugin;
        $plugin->getServer()->getPluginManager()->registerEvents($this, $plugin);
    }

}