<?php
/**
 * Created by PhpStorm.
 * User: AndrewBit
 * Date: 23/06/2016
 * Time: 19:31
 */

namespace DropIt;

use pocketmine\event\block\BlockBreakEvent;
use pocketmine\event\Listener;
use pocketmine\item\Item;

class Events implements Listener {

    /** @var Base */
    private $plugin;

    /**
     * Events constructor.
     * @param Base $plugin
     */
    public function __construct(Base $plugin) {
        $this->plugin = $plugin;
        $plugin->getServer()->getPluginManager()->registerEvents($this, $plugin);
    }

    /**
     * @param BlockBreakEvent $event
     */
    public function onBreak(BlockBreakEvent $event) {
        $id = $event->getBlock()->getId();
        $config = $this->plugin->getConfig();
        foreach($config->get("dropChanges") as $string) {
            $array = explode("=>", str_replace(" ", "", $string));
            if($id == $array[0]) {
                $drops = explode(",", $array[1]);
                $items = [];
                foreach($drops as $item){
                    $item = explode(":", $item);
                    $items[] = Item::get($item[0], 0, $item[1]);
                }
                $event->setDrops($items);
                break;
            }
        }
    }

}