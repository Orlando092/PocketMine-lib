<?php
/**
 * Created by PhpStorm.
 * User: AndrewBit
 * Date: 23/06/2016
 * Time: 19:30
 */

namespace DropIt;

use pocketmine\plugin\PluginBase;
use pocketmine\utils\Config;

class Base extends PluginBase {

    /** @var Events */
    private $listener;

    /** @var Config */
    private $config;

    /** @var Base */
    public static $object = null;

    public function onLoad() {
        if(!self::$object instanceof Base) {
            self::$object = $this;
        }
    }

    public function onEnable() {
        $this->initialize();
        $this->setConfig();
        $this->setListener();
        $this->getLogger()->info("DropIt was enabled.");
    }

    public function onDisable() {
        $this->getLogger()->info("DropIt was disabled.");
    }

    /**
     * @return Base
     */
    public static function getInstance() {
        return self::$object;
    }

    /**
     * @return Events
     */
    public function getListener() {
        return $this->listener;
    }

    public function setListener() {
        $this->listener = new Events($this);
    }

    /**
     * @return Config
     */
    public function getConfig() {
        return $this->config;
    }

    public function setConfig() {
        $this->config = new Config($this->getDataFolder() . "settings.yml");
    }

    public function initialize() {
        if(!is_dir($this->getDataFolder())) {
            mkdir($this->getDataFolder());
        }
        $this->saveResource("settings.yml");
    }

}